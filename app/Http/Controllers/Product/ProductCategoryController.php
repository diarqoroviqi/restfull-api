<?php

namespace App\Http\Controllers\Product;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ProductCategoryController extends ApiController
{

    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index']);
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $categories = $product->categories;

        return response()->json($categories, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Category $category)
    {
        //per me nderhy ne nje relacion many to many perdoren keto metoda
        // atach, sync, syncWithoutDetaching
        // atach e shton shum here
        //sync e shton niher edhe tjerat i fshin
        //syncWithoutDetaching e shton ni kategori e tjerat si fshin
        $product->categories()->syncWithoutDetaching([$category->id]);

        return $this->showAll($product->categories);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Category $category)
    {
        if (!$product->categories()->find($category->id)){
            return $this->errorResponse('The specified category is not category of this product', 404);
        }

        // detach() e fshin prej pivot table jo prej category table
        $product->categories()->detach($category->id);

        return $this->showAll($product->categories);
    }
}
