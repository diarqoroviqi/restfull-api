<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponser;
use Closure;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomThrottleRequests extends ThrottleRequests
{

    use ApiResponser;

    protected function buildException($key, $maxAttempts)
    {


        $retryAfter = $this->getTimeUntilNextRetry($key);

        $headers = $this->getHeaders(
            $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );

//       return new HttpException(
//           429, 'Too Many Attempts.', null, $headers
//        );

        return $this->errorResponse('Too Many Attempts', 409);

    }
}
