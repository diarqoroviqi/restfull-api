<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException){
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        if ($exception instanceof ModelNotFoundException){
            $modelName =strtolower(class_basename($exception->getModel()));
            return $this->errorResponse("Does not exists any {$modelName} with the specificated identificator", 404);
        }

        if ($exception instanceof AuthenticationException){
            return $this->unauthenticated($request, $exception);
        }

        // 403 e ndalon me kry naj veprim ne rastin ton e ndalon me bo sene qe nuk je i autorizum
        if ($exception instanceof AuthorizationException){
        return $this->errorResponse($exception->getMessage(), 403);
        }

        //405 Methoda e rekuestit nul lejohet psh get me post etj...
        if ($exception instanceof MethodNotAllowedHttpException){
            return $this->errorResponse('The specified method for the request is invalid', 405);
        }

        //404 page not found
        if ($exception instanceof NotFoundHttpException){
            return $this->errorResponse('The specified URL cannot found', 404);
        }

        if ($exception instanceof HttpException){
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        if ($exception instanceof QueryException) {
            $errorCode = $exception->errorInfo[1];

            // 409 http eshte per konflikt psh me foreign keys
            if  ($errorCode == 1451) {
                return $this->errorResponse('Cannot remove this source permanently. It is related with any other resource', 409);
            }
        }

        if ($exception instanceof  TokenMismatchException) {
            return redirect()->back()->withInput($request->input());
        }

        if (config('app.debug')){
            // 500 eshte excpetion nga ana e serverit nuk eshte faji i userit ose klientit
            return $this->errorResponse('Unexpected Exception. Try later', 500);
        }


        return parent::render($request, $exception);
    }

    // 401 kur nuk authenitkohesh dmth nuk logiratesh.
    protected function unauthenticated($request, AuthenticationException $exception){
        if ($this->isFrontEnd($request)) {
            return redirect()->back();
        }

        return $this->errorResponse('Unauthenticated', 401);
    }



    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {

       $errors =$e->validator->errors()->getMessages();

       if ($this->isFrontEnd($request)) {
          return  $request->ajax() ? response()->json($errors,422) : redirect()->back()
               ->withInput($request->input())
               ->withErrors($errors);
       }

       return $this->errorResponse($errors, 422);
    }

    private function isFrontEnd($request)
    {
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
}
